extern crate diesel;

use diesel::*;

use super::schema::*;

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[table_name = "genres"]
pub struct Genre {
    pub id: uuid::Uuid,
    pub tmdb_id: Option<i32>,
    pub name: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[table_name = "people"]
pub struct Person {
    pub id: uuid::Uuid,
    pub imdb_id: Option<String>,
    pub name: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[belongs_to(Person, foreign_key = "person_id")]
#[table_name = "title_casts"]
pub struct TitleCast {
    pub id: uuid::Uuid,
    pub category: Option<String>,
    pub characters: Vec<String>,
    pub credit: Option<String>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
    pub person_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[table_name = "title_descriptions"]
pub struct TitleDescription {
    pub id: uuid::Uuid,
    pub region: Option<String>,
    pub languages: Vec<String>,
    pub kind: String,
    pub overview: String,
    pub tagline: Option<String>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "show_id")]
#[table_name = "title_episodes"]
pub struct TitleEpisode {
    pub id: uuid::Uuid,
    pub show_id: uuid::Uuid,
    pub episode_id: uuid::Uuid,
    pub season_number: Option<String>,
    pub episode_number: Option<String>,
    pub air_date: Option<chrono::NaiveDate>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[belongs_to(Genre, foreign_key = "genre_id")]
#[table_name = "title_genres"]
pub struct TitleGenre {
    pub id: uuid::Uuid,
    pub title_id: uuid::Uuid,
    pub genre_id: uuid::Uuid,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[table_name = "title_images"]
pub struct TitleImage {
    pub id: uuid::Uuid,
    pub kind: String,
    pub language: Option<String>,
    pub mime: String,
    pub src: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[table_name = "title_media"]
pub struct TitleMedium {
    pub id: uuid::Uuid,
    pub mime: String,
    pub codecs: Vec<String>,
    pub languages: Vec<String>,
    pub src: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[table_name = "title_names"]
pub struct TitleName {
    pub id: uuid::Uuid,
    pub region: Option<String>,
    pub languages: Vec<String>,
    pub kind: String,
    pub name: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[table_name = "title_ratings"]
pub struct TitleRating {
    pub id: uuid::Uuid,
    pub region: Option<String>,
    pub certification: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[table_name = "title_subtitles"]
pub struct TitleSubtitle {
    pub id: uuid::Uuid,
    pub format: String,
    pub language: Option<String>,
    pub region: Option<String>,
    pub specifier: Option<String>,
    pub src: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Title, foreign_key = "title_id")]
#[table_name = "title_previews"]
pub struct TitlePreview {
    pub id: uuid::Uuid,
    pub src: Option<String>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub title_id: uuid::Uuid,
}

#[derive(Identifiable, Queryable, PartialEq, Debug)]
#[table_name = "titles"]
pub struct Title {
    pub id: uuid::Uuid,
    pub imdb_id: Option<String>,
    pub tmdb_id: Option<i32>,
    pub tvdb_id: Option<i32>,
    pub kind: String,
    pub original_language: Option<String>,
    pub runtime: Option<i32>,
    pub year_start: Option<i32>,
    pub year_end: Option<i32>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}
