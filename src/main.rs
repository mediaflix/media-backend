#![feature(proc_macro_hygiene, decl_macro)]

extern crate media_backend;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

use std::env;

use diesel::prelude::*;
use dotenv::dotenv;
use rocket::State;
use rocket_contrib::databases::diesel;
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;
use rocket_cors::AllowedHeaders;
use url::Url;

use media_backend::config::Config;
use media_backend::dto::*;
use media_backend::dto_helpers::{load_episodes, load_show, load_title, load_titles};
use media_backend::models::*;
use media_backend::param_helpers::ParamUuid;

#[database("mediaflix")]
struct MediaflixConnection(diesel::PgConnection);

#[get("/api/v1/genres")]
fn list_genres(db: MediaflixConnection) -> QueryResult<Json<Vec<GenreDto>>> {
    use media_backend::schema::*;
    let data = genres::table.load::<Genre>(&db.0)?;
    Ok(Json(data.into_iter().map(GenreDto::from).collect::<Vec<_>>()))
}

#[get("/api/v1/genres/<genre_id>")]
fn get_genre(db: MediaflixConnection, config: State<Config>, genre_id: ParamUuid) -> QueryResult<Json<GenreWithContentDto>> {
    use media_backend::schema::*;
    let genre: Genre = genres::table
        .find(genre_id.uuid())
        .first::<Genre>(&db.0)?;
    let titles: Vec<Title> = title_genres::table
        .filter(title_genres::genre_id.eq(genre.id))
        .inner_join(titles::table)
        .select(titles::all_columns)
        .load::<Title>(&db.0)?;
    Ok(Json(GenreWithContentDto {
        genre: GenreDto::from(genre),
        content: load_titles(&db.0, &config, titles)?,
    }))
}

#[get("/api/v1/content")]
fn list_content(db: MediaflixConnection, config: State<Config>) -> QueryResult<Json<Vec<ContentDto>>> {
    use media_backend::schema::*;
    let content = titles::table
        .left_outer_join(title_episodes::table.on(title_episodes::episode_id.eq(titles::id)))
        .filter(title_episodes::id.is_null())
        .select(titles::all_columns)
        .load::<Title>(&db.0)?;
    Ok(Json(load_titles(&db.0, &config, content)?))
}

#[get("/api/v1/content/<content_id>")]
fn get_content(db: MediaflixConnection, config: State<Config>, content_id: ParamUuid) -> QueryResult<Json<ContentMetaDto>> {
    use media_backend::schema::*;
    let content = load_title(
        &db.0,
        &config,
        titles::table
            .find(content_id.uuid())
            .first::<Title>(&db.0)?,
    )?;
    match content.kind.as_str() {
        "episode" => {
            let show = load_show(&db.0, &config, content.ids.uuid)?;
            Ok(Json(ContentMetaDto { content, instalment: Some(show) }))
        }
        _ => {
            Ok(Json(ContentMetaDto { content, instalment: None }))
        }
    }
}

#[get("/api/v1/content/<content_id>/episodes")]
fn list_episodes(db: MediaflixConnection, config: State<Config>, content_id: ParamUuid) -> QueryResult<Json<Vec<InstalmentDto>>> {
    Ok(Json(load_episodes(&db.0, &config, content_id.uuid())?))
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv().ok();

    rocket::ignite()
        .manage(Config {
            api_url: Url::parse(env::var("MEDIAFLIX_API_URL")?.as_str())?,
            static_url: Url::parse(env::var("MEDIAFLIX_STATIC_URL")?.as_str())?,
        })
        .mount(
            "/",
            rocket::routes![
                list_genres,
                get_genre,
                list_content,
                get_content,
                list_episodes
            ],
        )
        .mount("/media", StaticFiles::from(env::var("MEDIAFLIX_PATH")?))
        .attach(MediaflixConnection::fairing())
        .attach(rocket_cors::CorsOptions {
            allowed_headers: AllowedHeaders::some(&["Authorization", "Accept", "Accept-Language"]),
            allow_credentials: true,
            ..Default::default()
        }.to_cors()?)
        .launch();

    Ok(())
}
