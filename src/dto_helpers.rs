use diesel::*;
use url::ParseError;
use uuid::Uuid;

use crate::config::Config;
use crate::dto::*;
use crate::models::*;
use crate::schema::*;

pub fn load_episodes(db: &PgConnection, config: &Config, title_id: Uuid) -> QueryResult<Vec<InstalmentDto>> {
    let titles: Vec<(TitleEpisode, Title)> = title_episodes::table
        .filter(title_episodes::show_id.eq(title_id))
        .inner_join(titles::table.on(titles::id.eq(title_episodes::episode_id)))
        .order((title_episodes::season_number.asc(), title_episodes::episode_number.asc(), title_episodes::air_date.asc()))
        .load::<(TitleEpisode, Title)>(db)?;
    let (episodes, titles): (Vec<TitleEpisode>, Vec<Title>) = titles.into_iter().unzip();
    let titles = load_titles(db, config, titles)?;
    Ok(episodes.into_iter().zip(titles)
        .map(|(episode, title)| InstalmentDto::of(episode, title))
        .collect::<Vec<_>>())
}

pub fn load_show(db: &PgConnection, config: &Config, title_id: Uuid) -> QueryResult<InstalmentDto> {
    let (episode, show) = title_episodes::table
        .filter(title_episodes::episode_id.eq(title_id))
        .inner_join(titles::table.on(titles::id.eq(title_episodes::show_id)))
        .first::<(TitleEpisode, Title)>(db)?;
    Ok(InstalmentDto::of(episode, load_title(db, config, show)?))
}

pub fn load_title(db: &PgConnection, config: &Config, title: Title) -> QueryResult<ContentDto> {
    let title_names: Vec<TitleName> = TitleName::belonging_to(&title)
        .load::<TitleName>(db)?;
    let title_descriptions: Vec<TitleDescription> = TitleDescription::belonging_to(&title)
        .load::<TitleDescription>(db)?;
    let title_cast: Vec<(TitleCast, Person)> = TitleCast::belonging_to(&title)
        .inner_join(people::table)
        .load::<(TitleCast, Person)>(db)?;
    let title_genres: Vec<(TitleGenre, Genre)> = TitleGenre::belonging_to(&title)
        .inner_join(genres::table)
        .load::<(TitleGenre, Genre)>(db)?;
    let title_ratings: Vec<TitleRating> = TitleRating::belonging_to(&title)
        .load::<TitleRating>(db)?;
    let title_images: Vec<TitleImage> = TitleImage::belonging_to(&title)
        .load::<TitleImage>(db)?;
    let title_media: Vec<TitleMedium> = TitleMedium::belonging_to(&title)
        .load::<TitleMedium>(db)?;
    let title_subtitles: Vec<TitleSubtitle> = TitleSubtitle::belonging_to(&title)
        .load::<TitleSubtitle>(db)?;
    let title_preview: Option<TitlePreview> = TitlePreview::belonging_to(&title)
        .first::<TitlePreview>(db)
        .optional()?;
    Ok(process_title(
        config,
        title, title_names, title_descriptions,
        title_cast, title_genres, title_ratings,
        title_images, title_media, title_subtitles,
        title_preview,
    ))
}

pub fn load_titles(db: &PgConnection, config: &Config, titles: Vec<Title>) -> QueryResult<Vec<ContentDto>> {
    let title_names: Vec<Vec<TitleName>> = TitleName::belonging_to(&titles)
        .load::<TitleName>(db)?
        .grouped_by(&titles);
    let title_descriptions: Vec<Vec<TitleDescription>> = TitleDescription::belonging_to(&titles)
        .load::<TitleDescription>(db)?
        .grouped_by(&titles);
    let title_cast: Vec<Vec<(TitleCast, Person)>> = TitleCast::belonging_to(&titles)
        .inner_join(people::table)
        .load::<(TitleCast, Person)>(db)?
        .grouped_by(&titles);
    let title_genres: Vec<Vec<(TitleGenre, Genre)>> = TitleGenre::belonging_to(&titles)
        .inner_join(genres::table)
        .load::<(TitleGenre, Genre)>(db)?
        .grouped_by(&titles);
    let title_ratings: Vec<Vec<TitleRating>> = TitleRating::belonging_to(&titles)
        .load::<TitleRating>(db)?
        .grouped_by(&titles);
    let title_images: Vec<Vec<TitleImage>> = TitleImage::belonging_to(&titles)
        .load::<TitleImage>(db)?
        .grouped_by(&titles);
    let title_media: Vec<Vec<TitleMedium>> = TitleMedium::belonging_to(&titles)
        .load::<TitleMedium>(db)?
        .grouped_by(&titles);
    let title_subtitles: Vec<Vec<TitleSubtitle>> = TitleSubtitle::belonging_to(&titles)
        .load::<TitleSubtitle>(db)?
        .grouped_by(&titles);
    let title_preview: Vec<Option<TitlePreview>> = TitlePreview::belonging_to(&titles)
        .load::<TitlePreview>(db)?
        .grouped_by(&titles)
        .into_iter()
        .map(|vec: Vec<TitlePreview>| vec.into_iter().next())
        .collect::<Vec<_>>();
    Ok(titles.into_iter()
        .zip(title_names)
        .zip(title_descriptions)
        .zip(title_cast)
        .zip(title_genres)
        .zip(title_ratings)
        .zip(title_images)
        .zip(title_media)
        .zip(title_subtitles)
        .zip(title_preview)
        .map(|tuple| {
            let (((((((((title, akas), descriptions),
                cast), genres), ratings),
                images), media), subtitles), preview) = tuple;
            process_title(
                config,
                title, akas, descriptions,
                cast, genres, ratings,
                images, media, subtitles,
                preview,
            )
        }).collect::<Vec<ContentDto>>())
}

fn process_title(
    config: &Config,
    title: Title, names: Vec<TitleName>, descriptions: Vec<TitleDescription>,
    cast: Vec<(TitleCast, Person)>, genres: Vec<(TitleGenre, Genre)>, ratings: Vec<TitleRating>,
    images: Vec<TitleImage>, media: Vec<TitleMedium>, subtitles: Vec<TitleSubtitle>,
    preview: Option<TitlePreview>,
) -> ContentDto {
    ContentDto::of(
        title,
        names.into_iter().map(|src| {
            ContentNameDto::from(src)
        }).collect::<Vec<_>>(),
        descriptions.into_iter().map(|src| {
            ContentDescriptionDto::from(src)
        }).collect::<Vec<_>>(),
        cast.into_iter().map(|src| {
            let (cast, person) = src;
            CastDto::of(cast, person)
        }).collect::<Vec<_>>(),
        genres.into_iter().map(|(_, src)| {
            GenreDto::from(src)
        }).collect::<Vec<_>>(),
        ratings.into_iter().map(|src| {
            RatingDto::from(src)
        }).collect::<Vec<_>>(),
        images.into_iter().filter_map(|src| {
            ImageDto::of(src, &config).ok()
        }).collect::<Vec<_>>(),
        media.into_iter().filter_map(|src| {
            MediaDto::of(src, &config).ok()
        }).collect::<Vec<_>>(),
        subtitles.into_iter().filter_map(|src| {
            SubtitleDto::of(src, &config).ok()
        }).collect::<Vec<_>>(),
        preview
            .and_then(|preview| preview.src)
            .and_then(|preview| absolute_url(config, UrlKind::Static, preview).ok()),
    )
}

pub enum UrlKind {
    Static,
    Api,
}

pub fn absolute_url(config: &Config, url_kind: UrlKind, url: impl Into<String>) -> Result<String, ParseError> {
    let base_url = match url_kind {
        UrlKind::Static => &config.static_url,
        UrlKind::Api => &config.api_url
    };
    base_url.join(url.into().as_str()).map(url::Url::into_string)
}