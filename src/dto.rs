use serde::{Deserialize, Serialize};

use crate::config::Config;
use crate::dto_helpers::{absolute_url, UrlKind};
use crate::models::*;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ContentDto {
    pub ids: ContentIdDto,
    pub kind: String,
    pub original_language: Option<String>,
    pub runtime: Option<i32>,
    pub year_start: Option<i32>,
    pub year_end: Option<i32>,
    pub titles: Vec<ContentNameDto>,
    pub descriptions: Vec<ContentDescriptionDto>,
    pub cast: Vec<CastDto>,
    pub genres: Vec<GenreDto>,
    pub ratings: Vec<RatingDto>,
    pub images: Vec<ImageDto>,
    pub media: Vec<MediaDto>,
    pub subtitles: Vec<SubtitleDto>,
    pub preview: Option<String>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

impl ContentDto {
    pub fn of(
        src: Title,
        titles: Vec<ContentNameDto>,
        descriptions: Vec<ContentDescriptionDto>,
        cast: Vec<CastDto>,
        genres: Vec<GenreDto>,
        ratings: Vec<RatingDto>,
        images: Vec<ImageDto>,
        media: Vec<MediaDto>,
        subtitles: Vec<SubtitleDto>,
        preview: Option<String>,
    ) -> Self {
        ContentDto {
            ids: ContentIdDto {
                uuid: src.id,
                imdb: src.imdb_id,
                tmdb: src.tmdb_id,
                tvdb: src.tvdb_id,
            },
            kind: src.kind,
            original_language: src.original_language,
            runtime: src.runtime,
            year_start: src.year_start,
            year_end: src.year_end,
            titles,
            descriptions,
            cast,
            genres,
            ratings,
            images,
            media,
            subtitles,
            preview,
            created_at: src.created_at,
            updated_at: src.updated_at,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ContentIdDto {
    pub uuid: uuid::Uuid,
    pub imdb: Option<String>,
    pub tmdb: Option<i32>,
    pub tvdb: Option<i32>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ContentNameDto {
    pub region: Option<String>,
    pub languages: Vec<String>,
    pub kind: String,
    pub name: String,
}

impl From<TitleName> for ContentNameDto {
    fn from(src: TitleName) -> Self {
        ContentNameDto {
            region: src.region,
            languages: src.languages,
            kind: src.kind,
            name: src.name,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ContentDescriptionDto {
    pub region: Option<String>,
    pub languages: Vec<String>,
    pub kind: String,
    pub overview: String,
    pub tagline: Option<String>,
}

impl From<TitleDescription> for ContentDescriptionDto {
    fn from(src: TitleDescription) -> Self {
        ContentDescriptionDto {
            region: src.region,
            languages: src.languages,
            kind: src.kind,
            overview: src.overview,
            tagline: src.tagline,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct GenreDto {
    pub id: uuid::Uuid,
    pub tmdb_id: Option<i32>,
    pub name: String,
}

impl From<Genre> for GenreDto {
    fn from(src: Genre) -> Self {
        GenreDto {
            id: src.id,
            tmdb_id: src.tmdb_id,
            name: src.name,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct CastDto {
    pub category: Option<String>,
    pub characters: Vec<String>,
    pub credit: Option<String>,
    pub person: PersonDto,
}

impl CastDto {
    pub fn of(cast: TitleCast, person: Person) -> Self {
        CastDto {
            category: cast.category,
            characters: cast.characters,
            credit: cast.credit,
            person: PersonDto::from(person),
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct PersonDto {
    pub id: uuid::Uuid,
    pub imdb_id: Option<String>,
    pub name: String,
}

impl From<Person> for PersonDto {
    fn from(src: Person) -> Self {
        PersonDto {
            id: src.id,
            imdb_id: src.imdb_id,
            name: src.name,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct InstalmentDto {
    pub season: Option<String>,
    pub episode: Option<String>,
    pub air_date: Option<chrono::NaiveDate>,
    pub content: ContentDto,
}

impl InstalmentDto {
    pub fn of(episode: TitleEpisode, content: ContentDto) -> Self {
        InstalmentDto {
            season: episode.season_number,
            episode: episode.episode_number,
            air_date: episode.air_date,
            content,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ImageDto {
    pub kind: String,
    pub language: Option<String>,
    pub mime: String,
    pub src: String,
}

impl ImageDto {
    pub fn of(src: TitleImage, config: &Config) -> Result<Self, url::ParseError> {
        Ok(ImageDto {
            kind: src.kind,
            language: src.language,
            mime: src.mime,
            src: absolute_url(config, UrlKind::Static, src.src)?,
        })
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct MediaDto {
    pub mime: String,
    pub codecs: Vec<String>,
    pub languages: Vec<String>,
    pub src: String,
}

impl MediaDto {
    pub fn of(src: TitleMedium, config: &Config) -> Result<Self, url::ParseError> {
        Ok(MediaDto {
            mime: src.mime,
            codecs: src.codecs,
            languages: src.languages,
            src: absolute_url(config, UrlKind::Static, src.src)?,
        })
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RatingDto {
    pub region: Option<String>,
    pub certification: String,
}

impl From<TitleRating> for RatingDto {
    fn from(src: TitleRating) -> Self {
        RatingDto {
            region: src.region,
            certification: src.certification,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SubtitleDto {
    pub format: String,
    pub language: Option<String>,
    pub region: Option<String>,
    pub specifier: Option<String>,
    pub src: String,
}

impl SubtitleDto {
    pub fn of(src: TitleSubtitle, config: &Config) -> Result<Self, url::ParseError> {
        Ok(SubtitleDto {
            format: src.format,
            language: src.language,
            region: src.region,
            specifier: src.specifier,
            src: absolute_url(config, UrlKind::Static, src.src)?,
        })
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct GenreWithContentDto {
    pub genre: GenreDto,
    pub content: Vec<ContentDto>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ContentMetaDto {
    pub content: ContentDto,
    pub instalment: Option<InstalmentDto>,
}