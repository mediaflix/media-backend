table! {
    genres (id) {
        id -> Uuid,
        tmdb_id -> Nullable<Int4>,
        name -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    people (id) {
        id -> Uuid,
        imdb_id -> Nullable<Text>,
        name -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    title_casts (id) {
        id -> Uuid,
        category -> Nullable<Text>,
        characters -> Array<Text>,
        credit -> Nullable<Text>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
        person_id -> Uuid,
    }
}

table! {
    title_descriptions (id) {
        id -> Uuid,
        region -> Nullable<Text>,
        languages -> Array<Text>,
        kind -> Text,
        overview -> Text,
        tagline -> Nullable<Text>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
    }
}

table! {
    title_episodes (id) {
        id -> Uuid,
        show_id -> Uuid,
        episode_id -> Uuid,
        season_number -> Nullable<Text>,
        episode_number -> Nullable<Text>,
        air_date -> Nullable<Date>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    title_genres (id) {
        id -> Uuid,
        title_id -> Uuid,
        genre_id -> Uuid,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    title_images (id) {
        id -> Uuid,
        kind -> Text,
        language -> Nullable<Text>,
        mime -> Text,
        src -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
    }
}

table! {
    title_media (id) {
        id -> Uuid,
        mime -> Text,
        codecs -> Array<Text>,
        languages -> Array<Text>,
        src -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
    }
}

table! {
    title_names (id) {
        id -> Uuid,
        region -> Nullable<Text>,
        languages -> Array<Text>,
        kind -> Text,
        name -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
    }
}

table! {
    title_ratings (id) {
        id -> Uuid,
        region -> Nullable<Text>,
        certification -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
    }
}

table! {
    title_subtitles (id) {
        id -> Uuid,
        format -> Text,
        language -> Nullable<Text>,
        region -> Nullable<Text>,
        specifier -> Nullable<Text>,
        src -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
    }
}

table! {
    title_previews (id) {
        id -> Uuid,
        src -> Nullable<Text>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title_id -> Uuid,
    }
}

table! {
    titles (id) {
        id -> Uuid,
        imdb_id -> Nullable<Text>,
        tmdb_id -> Nullable<Int4>,
        tvdb_id -> Nullable<Int4>,
        kind -> Text,
        original_language -> Nullable<Text>,
        runtime -> Nullable<Int4>,
        year_start -> Nullable<Int4>,
        year_end -> Nullable<Int4>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(title_casts -> people (person_id));
joinable!(title_casts -> titles (title_id));
joinable!(title_descriptions -> titles (title_id));
joinable!(title_genres -> genres (genre_id));
joinable!(title_genres -> titles (title_id));
joinable!(title_images -> titles (title_id));
joinable!(title_media -> titles (title_id));
joinable!(title_names -> titles (title_id));
joinable!(title_ratings -> titles (title_id));
joinable!(title_subtitles -> titles (title_id));
joinable!(title_previews -> titles (title_id));

allow_tables_to_appear_in_same_query!(
    genres,
    people,
    title_casts,
    title_descriptions,
    title_episodes,
    title_genres,
    title_images,
    title_media,
    title_names,
    title_ratings,
    title_subtitles,
    titles,
);
