use url::Url;

pub struct Config {
    pub api_url: Url,
    pub static_url: Url,
}