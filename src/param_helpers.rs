use rocket::request::FromParam;
use rocket::http::RawStr;
use uuid::Uuid;

pub struct ParamUuid(uuid::Uuid);

impl ParamUuid {
    pub fn uuid(&self) -> uuid::Uuid {
        self.0
    }
}

impl FromParam<'_> for ParamUuid {
    type Error = uuid::Error;

    fn from_param(param: &RawStr) -> Result<Self, Self::Error> {
        return Ok(ParamUuid(Uuid::parse_str(param.as_str())?));
    }
}