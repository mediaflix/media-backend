alter table titles drop constraint title_parent_id_fkey;
drop table title_subtitles;
drop table title_ratings;
drop table title_names;
drop table title_media;
drop table title_images;
drop table title_genres;
drop table title_episodes;
drop table title_descriptions;
drop table title_casts;
drop table titles;
drop table people;
drop table genres;
